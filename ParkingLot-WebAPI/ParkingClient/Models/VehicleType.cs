﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingClient.Models
{
    enum VehicleType
    {
        Car,
        Bus,
        Motorcycle,
        Truck
    }
}
