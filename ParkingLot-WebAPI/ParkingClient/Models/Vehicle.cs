﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace ParkingClient.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    struct Vehicle
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }
        [JsonProperty("balance")]
        public decimal Balance { get; set; }
        [JsonProperty("type")]
        public VehicleType Type { get; set; }
        public Vehicle(VehicleType Type, decimal Balance, Guid Id = new Guid())
        {
            this.Id = Id;
            this.Type = Type;
            this.Balance = Balance;
        }
        public override string ToString()
        {
            return $"ID: {Id}, Vehicle Type: {Type.ToString()}, Balance: {Balance}";
        }
    }
}
