﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace ParkingClient.Models
{
    struct TopUpBalanceModel
    {
        public Guid Id { get; set; }
        public decimal Balance { get; set; }
        public TopUpBalanceModel(Guid id, decimal balance)
        {
            Id = id;
            Balance = balance;
        }
    }
}
