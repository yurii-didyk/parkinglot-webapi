﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace ParkingClient.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    struct Transaction
    {
        [JsonProperty("time")]
        public DateTime Time { get; set; }
        [JsonProperty("vehicleId")]
        public Guid Id { get; set; }
        [JsonProperty("ticksToStartFine")]
        public double Ticks { get; set; }
        [JsonProperty("withdraw")]
        public decimal Withdraw { get; set; }
        public override string ToString()
        {
            if (Ticks > 0)
                return $"VehicleId: {Id}; Time: {Time}; Withdrawn money: {Withdraw}; Vehicle can be parked for {String.Format("{0:00.0}", Ticks)} ticks before vehicle`s balance become negative";
            else
            {
                return $"VehicleId: {Id}; Time: {Time}; Withdrawn money: {Withdraw}; Warning: vehicle`s balance is negative, please, top up vehicle`s balance";
            }
        }
    }
}
