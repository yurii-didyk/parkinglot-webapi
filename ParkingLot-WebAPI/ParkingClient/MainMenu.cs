﻿using System;
using System.Collections.Generic;
using System.Text;
using ParkingClient.Models;
using ParkingClient;
using System.IO;
using System.Net.Http;
using System.Net;
using System.Threading.Tasks;
using System.Threading;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;
using System.Net.Http.Headers;


namespace ParkingClient
{

    class MainMenu : IMenu
    {
        static HttpClient client = new HttpClient();

        public MainMenu()
        {
            client.BaseAddress = new Uri("http://localhost:5000/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        static async Task<IEnumerable<string>> GetConfigurationAsync()
        {
            IEnumerable<string> settings = null;
            HttpResponseMessage response = await client.GetAsync("api/settings");
            response.EnsureSuccessStatusCode();
            if (response.IsSuccessStatusCode)
            {
                settings = await response.Content.ReadAsAsync<IEnumerable<string>>();
            }
            return settings;
        }

        static async Task<Uri> AddVehicleAsync(Vehicle? vehicle)
        {
            HttpResponseMessage response = await client.PostAsJsonAsync(
              "api/vehicles", vehicle);
            response.EnsureSuccessStatusCode();
            return response.Headers.Location;
        }

        static async Task<IEnumerable<Vehicle>> GetVehiclesAsync()
        {
            IEnumerable<Vehicle> vehicles = null;
            HttpResponseMessage response = await client.GetAsync("api/vehicles");
            response.EnsureSuccessStatusCode();
            if (response.IsSuccessStatusCode)
            {
                vehicles = await response.Content.ReadAsAsync<IEnumerable<Vehicle>>();
            }

            return vehicles;
        }

        static async Task<Vehicle?> GetVehicleAsync(Guid id)
        {
            Vehicle? vehicle = null;
            HttpResponseMessage response = await client.GetAsync($"api/vehicles/{id}");
            response.EnsureSuccessStatusCode();
            if (response.IsSuccessStatusCode)
            {
                vehicle = await response.Content.ReadAsAsync<Vehicle>();
            }

            return vehicle;
        }

        static async Task<decimal?> GetParkingBalanceAsync()
        {
            decimal? result = null;
            HttpResponseMessage response = await client.GetAsync($"api/parking/balance");
            response.EnsureSuccessStatusCode();
            if (response.IsSuccessStatusCode)
            {
                result = await response.Content.ReadAsAsync<decimal>();
            }

            return result;
        }

        static async Task<decimal?> GetLastMinuteParkingBalanceAsync()
        {
            decimal? result = null;
            HttpResponseMessage response = await client.GetAsync($"api/parking/lastminutebalance");
            response.EnsureSuccessStatusCode();
            if (response.IsSuccessStatusCode)
            {
                result = await response.Content.ReadAsAsync<decimal>();
            }

            return result;
        }

        static async Task DeleteVehicleAsync(Guid id)
        {
            HttpResponseMessage response = await client.DeleteAsync($"api/vehicles/{id}");
        }

        static async Task<IEnumerable<string>> GetTransactionsAsync()
        {
            IEnumerable<string> transactions = null;
            HttpResponseMessage response = await client.GetAsync($"api/transactions");
            response.EnsureSuccessStatusCode();
            if (response.IsSuccessStatusCode)
            {
                transactions = await response.Content.ReadAsAsync<IEnumerable<string>>();
            }
            return transactions;
        }

        static async Task TopUpVehicleBalanceAsync(TopUpBalanceModel model)
        {
            HttpResponseMessage response = await client.PutAsJsonAsync($"api/vehicles/topup", model);
        }

        static async Task<IEnumerable<Transaction>> GetLastMinuteTransactionsAsync()
        {
            IEnumerable<Transaction> transactions = null;
            HttpResponseMessage response = await client.GetAsync($"api/transactions/getlastminutetransactions");
            response.EnsureSuccessStatusCode();
            if (response.IsSuccessStatusCode)
            {
                transactions = await response.Content.ReadAsAsync<IEnumerable<Transaction>>();
            }
            return transactions;
        }

        static async Task<Dictionary<string, uint>> GetParkingPlaces()
        {
            Dictionary<string, uint> places = null;
            HttpResponseMessage response = await client.GetAsync($"api/parking/parkingplaces");
            response.EnsureSuccessStatusCode();
            if (response.IsSuccessStatusCode)
            {
                places = await response.Content.ReadAsAsync<Dictionary<string, uint>>();
            }

            return places;
        }
    
        private readonly string menu = "\nPlease, choose action which you want to run...\n1. Current parking balance\n2. Cash earned by parking during last minute\n3. Show parking places(all - busy - free)\n4. Show vehicle info\n5. Add vehicle\n6. Remove vehicle\n7. Top up vehicle`s balance\n8. Show last minute transactions\n9. Show .log file\n10. Show all vehicles\n11. Show configuration\n0. Exit\n";
        public void ShowMenu()
        {
            Console.WriteLine(menu);
        }
        public async Task Action()
        {
            
            ushort choice;
            Console.Clear();
            ShowMenu();
            bool valid = UInt16.TryParse(Console.ReadLine(), out choice);
            if (!valid)
            {
                
                throw new UnknownCommandException("Please input a valid number...");
            }
            switch (choice)
            {
                case 1:
                    try
                    {
                        decimal? balance = await GetParkingBalanceAsync();
                        Console.WriteLine($"Parking balance now: {balance}");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    break;
                case 2:
                    decimal? lastminuteBalance = await GetLastMinuteParkingBalanceAsync();
                    Console.WriteLine($"Cash earned by parking for last minute: {lastminuteBalance}");
                    break;
                case 3:
                    Dictionary<string,uint> places = await GetParkingPlaces();
                    foreach(KeyValuePair<string, uint> place in places)
                    {
                        Console.WriteLine($"{place.Value} {place.Key} parking places");
                    }
                    break;
                case 4:
                    Console.WriteLine("Input vehicle`s id number: ");
                    Guid id;
                    Guid.TryParse(Console.ReadLine(), out id);
                    Vehicle? vehicle = await GetVehicleAsync(id);
                    if (vehicle == null)
                    {
                        Console.WriteLine("Vehicle with this id not founded...");
                    }
                    Console.WriteLine(vehicle.ToString());
                    break;
                case 5:
                    Console.WriteLine("\nAvailable vehicle type:");
                    foreach (var vType in Enum.GetValues(typeof(VehicleType)))
                    {
                        Console.WriteLine($" {(int)vType}. {vType}");
                    }
                    int type = 0;
                    Console.Write("Enter type(number): ");
                    bool isValid = Int32.TryParse(Console.ReadLine(), out type);
                    if (!isValid || !Enum.IsDefined(typeof(VehicleType), type))
                    {
                        Console.WriteLine("Please, input number between 0 and 3...");
                        return;
                    }
                    decimal value = 0;
                    Console.Write("Enter amount of starter money: ");
                    Decimal.TryParse(Console.ReadLine(), out value);
                    vehicle = new Vehicle((VehicleType)type, value);
                    try
                    {
                        await AddVehicleAsync(vehicle);
                    }
                    catch (InvalidOperationException ex)
                    {
                        Console.WriteLine("Error");
                        Console.WriteLine(ex.Message);
                        Timeout();
                        return;
                    }
                    break;
                case 6:
                    Console.Write("Enter your VehicleID: ");
                    Guid.TryParse(Console.ReadLine(), out id);
                    await DeleteVehicleAsync(id);
                    break;
                case 7:
                    Console.Write("Enter your VehicleID: ");
                    Guid.TryParse(Console.ReadLine(), out id);
                    Console.Write("Enter amount: ");
                    Decimal.TryParse(Console.ReadLine(), out value);
                    await TopUpVehicleBalanceAsync(new TopUpBalanceModel(id, value));
                    break;
                case 8:
                    IEnumerable<Transaction> transactions = await GetLastMinuteTransactionsAsync();
                    foreach (Transaction item in transactions)
                    {
                        Console.WriteLine(item.ToString());
                    }
                    break;
                case 9:
                    IEnumerable<string> transactionsContainer = await GetTransactionsAsync();
                    Console.WriteLine("Transactions.log");
                    Console.WriteLine();
                    foreach (var item in transactionsContainer)
                    {
                        Console.WriteLine(item);
                    }
                    break;
                case 10:
                    var itemsList = await GetVehiclesAsync();
                    foreach (Vehicle item in itemsList)
                    {
                        Console.WriteLine(item.ToString());
                    }
                    break;
                case 11:
                    Console.WriteLine();
                    var settings = await GetConfigurationAsync();
                    foreach (string item in settings)
                    {
                        Console.WriteLine(item);
                    }
                    break;
                case 0:
                    System.Diagnostics.Process.GetCurrentProcess().Kill();
                    break;
                default:
                    Console.WriteLine("Please input valid number...");
                    break;
            }
        }
        private void Timeout()
        {
            Console.ReadKey();
            Console.Clear();
        }
        public void Run()
        {
            while (true)
            {
 
                try
                {
                    Task.WaitAll();
                    Action();
                }
                catch (UnknownCommandException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (ParkingFullException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }
        
    }
}
