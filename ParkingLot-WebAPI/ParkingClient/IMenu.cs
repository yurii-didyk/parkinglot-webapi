﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ParkingClient
{
    interface IMenu
    {
        void ShowMenu();
        Task Action();
        void Run();
    }
}
