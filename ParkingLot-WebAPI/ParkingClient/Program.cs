﻿using System;

namespace ParkingClient
{
    class Program
    {
        static void Main(string[] args)
        {
            IMenu menu = new MainMenu();
            menu.Run();
        }
    }
}
