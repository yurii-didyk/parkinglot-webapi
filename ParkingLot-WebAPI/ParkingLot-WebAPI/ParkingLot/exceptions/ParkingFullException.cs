﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingLot_WebAPI
{
    public class ParkingFullException: Exception
    {
        public ParkingFullException()
        {
        }

        public ParkingFullException(string message)
            : base(message)
        {
        }

        public ParkingFullException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
