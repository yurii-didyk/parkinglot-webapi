﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingLot_WebAPI.Models
{
    public static class Settings
    {
        public static decimal StarterBalance { get; set; }
        public static uint ParkingPlaces { get; set; }
        public static float FineRatio { get; set; }
        public static uint PaymentInterval { get; set; }
        public static uint LoggingInterval { get; set; }
        public static string LoggingFilePath { get; set; }

        public static Dictionary<VehicleType, decimal> PricesDictionary { get; set; } = new Dictionary<VehicleType, decimal>()
        {
            [VehicleType.Car] = 2M,
            [VehicleType.Bus] = 3.5M,
            [VehicleType.Truck] = 5M,
            [VehicleType.Motorcycle] = 1M,
        };
        static Settings()
        {
            StarterBalance = 0;
            ParkingPlaces = 10;
            PaymentInterval = 5000; // 5 seconds
            LoggingInterval = 60000; // 60 seconds
            FineRatio = 2.5f;
            LoggingFilePath = System.Reflection.Assembly.GetExecutingAssembly().Location + @"\..\logs\Transactions.log";
        }

    }
}
