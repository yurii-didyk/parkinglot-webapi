﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingLot_WebAPI.Models
{
    public enum VehicleType
    {
        Car,
        Bus,
        Truck,
        Motorcycle
    }
}
