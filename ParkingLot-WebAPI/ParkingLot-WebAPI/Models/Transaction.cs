﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingLot_WebAPI.Models
{
   public class Transaction
    {
        public DateTime Time { get; private set; }
        public Guid VehicleId { get; private set; }
        public decimal Withdraw { get; private set; }
        public double TicksToStartFine { get; private set; }
        public Transaction(Guid VehicleId, decimal Withdraw, double TicksToStartFine)
        {
            Time = DateTime.Now;
            this.VehicleId = VehicleId;
            this.Withdraw = Withdraw;
            this.TicksToStartFine = TicksToStartFine;
        }
        public override string ToString()
        {
            if (TicksToStartFine > 0)
                return $"VehicleId: {VehicleId}; Time: {Time}; Withdrawn money: {Withdraw}; Vehicle can be parked for {TicksToStartFine} ticks before vehicle`s balance become negative";
            else
            {
                return $"VehicleId: {VehicleId}; Time: {Time}; Withdrawn money: {Withdraw}; Warning: vehicle`s balance is negative, please, top up vehicle`s balance";
            }
        }
    }
}
