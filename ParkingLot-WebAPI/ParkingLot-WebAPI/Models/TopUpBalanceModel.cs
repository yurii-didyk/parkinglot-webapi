﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParkingLot_WebAPI.Models
{
    public class TopUpBalanceModel
    {
        public Guid Id { get; set; }
        public decimal Balance { get; set; }
        public TopUpBalanceModel(Guid Id, decimal Balance)
        {
            this.Id = Id;
            this.Balance = Balance;
        }
    }
}
