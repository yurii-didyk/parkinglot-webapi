﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ParkingLot_WebAPI.Services;
using ParkingLot_WebAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ParkingLot_WebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class SettingsController : ControllerBase
    {
        readonly SettingsService _service = new SettingsService();
        [HttpGet]
        public IActionResult GetSettingsConfiguration()
        {
            var settings = _service.GetConfiguration();
            if (settings != null)
            {
                return new JsonResult(settings);
            }
            return NotFound();
        }
    }
}