﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ParkingLot_WebAPI.Models;
using ParkingLot_WebAPI.Services;

namespace ParkingLot_WebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private readonly ParkingService _service;

        public ParkingController(ParkingService parkingService)
        {
            _service = parkingService;
        }

        [HttpGet]
        public IDictionary<string, uint> ParkingPlaces()
        {
            return _service.GetParkingPlaces();
        }
        [HttpGet]
        public decimal Balance()
        {
            return _service.GetBalance();
        }
        [HttpGet]
        public decimal LastMinuteBalance()
        {
            return _service.LastMinuteBalance();
        }
    }
}