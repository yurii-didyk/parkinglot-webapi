﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ParkingLot_WebAPI.Models;
using ParkingLot_WebAPI.Services;

namespace ParkingLot_WebAPI
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly VehiclesService _service;
        public VehiclesController(VehiclesService vehicelesService)
        {
            _service = vehicelesService;
        }
        [HttpGet]
        public IEnumerable<Vehicle> GetVehicles()
        {
            return _service.GetVehicles();
        }
        [HttpGet("{id}")]
        public Vehicle GetVehicle(Guid id)
        {
            
            return _service.GetVehicle(id);
        }
        [HttpPost]
        public IActionResult AddVehicle([FromBody]Vehicle vehicle)
        {
            if (vehicle == null)
            {
                return BadRequest();
            }

            var item = _service.AddVehicle(vehicle);
            return Ok();
        }
        [HttpDelete("{id}")]
        public IActionResult DeleteVehicle(Guid id)
        {
            try
            {
                _service.DeleteVehicle(id);
                return NoContent();
            }
            catch (RemoveVehicleWithFineException)
            {
                return StatusCode(403, "Vehicle can`t be removed from parking! It has fine...");
            }
            catch (Exception e)
            {
                return StatusCode(403, e.Message);
            }
        }
        [HttpPut("topup")]
        public IActionResult TopUpVehicleBalance([FromBody]TopUpBalanceModel model)
        {
            try
            {
                _service.TopUpVehicleBalance(model.Id, model.Balance);
                return NoContent();
            }
            catch (Exception e)
            {
                return StatusCode(403, e.Message);
            }
        }
    }
}