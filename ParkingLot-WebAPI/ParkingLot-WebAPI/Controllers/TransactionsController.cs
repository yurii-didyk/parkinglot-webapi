﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ParkingLot_WebAPI.Services;
using ParkingLot_WebAPI.Models;

namespace ParkingLot_WebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly TransactionsService _service;
        public TransactionsController(TransactionsService transactionsService)
        {
            _service = transactionsService;
            //Parking.Source.Active();
        }
        [HttpGet]
        public IEnumerable<string> GetTransactions()
        {
            var transactions = _service.GetTransactions();
            if (transactions != null)
                return _service.GetTransactions();
            return new List<string>() { "Nothing found!"};
        }
        [HttpGet("{id}")]
        public Transaction GetTransaction(Guid id)
        {
                return _service.GetTransaction(id);
        }
        [HttpGet("[action]")]
        public IEnumerable<Transaction> GetLastMinuteTransactions()
        {
                return _service.GetLastMinuteTransactions();
        }
        
    }
}