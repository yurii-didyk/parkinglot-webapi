﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ParkingLot_WebAPI.Models;
using AutoMapper;

namespace ParkingLot_WebAPI.Services
{
    public class ParkingService
    {

        public IDictionary<string, uint> GetParkingPlaces()
        {
            Dictionary<string, uint> places = new Dictionary<string, uint>
            {
                ["free"] = Parking.Source.FreeParkingPlaces,
                ["total"] = Parking.Source.ParkingPlaces,
                ["busy"] = Parking.Source.ParkingPlaces - Parking.Source.FreeParkingPlaces

            };
            return places;
        }


        public decimal GetBalance()
        {
            return Parking.Source.Balance;
        }


        public decimal LastMinuteBalance()
        {
            return Parking.Source.BalanceForLastMinute;
        }
    }
}
