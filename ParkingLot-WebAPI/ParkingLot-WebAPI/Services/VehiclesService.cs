﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ParkingLot_WebAPI.Models;
using AutoMapper;

namespace ParkingLot_WebAPI.Services
{
    public class VehiclesService
    {
        public IEnumerable<Vehicle> GetVehicles()
        {
            return Parking.Source.GetVehicles;
        }


        public Vehicle GetVehicle(Guid id)
        {
            Vehicle item = null;
            var vehicles = Parking.Source.GetVehicles;
            try
            {
                item = vehicles.First(v => v.Id == id);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return item;
        }


        public void TopUpVehicleBalance(Guid vehicleId, decimal balance)
        {
            Parking.Source.TopUpVehicleBalance(vehicleId, balance);
        }


        public Vehicle AddVehicle(Vehicle vehicle)
        {
            Parking.Source.AddVehicle(vehicle);
            return vehicle;
        }


        public void DeleteVehicle(Guid id)
        {
            Parking.Source.RemoveVehicle(id);
        }
    }
}
