﻿using System;
using System.Collections.Generic;
using System.Linq;
using ParkingLot_WebAPI;
using ParkingLot_WebAPI.Models;
using System.Threading.Tasks;

namespace ParkingLot_WebAPI.Services
{
    public class SettingsService
    {
        public IEnumerable<string> GetConfiguration()
        {
            List<string> properties = new List<string>
            {
                $"Starter balance: {Settings.StarterBalance}",
                $"Parking places: {Settings.ParkingPlaces}",
                $"Fine ratio: {Settings.FineRatio}",
                $"Payment interval: {Settings.PaymentInterval} miliseconds",
                $"Logging interval: {Settings.LoggingInterval} miliseconds",
                $"Logging path: {Settings.LoggingFilePath}",
            };
            return properties;
        }
    }
}
