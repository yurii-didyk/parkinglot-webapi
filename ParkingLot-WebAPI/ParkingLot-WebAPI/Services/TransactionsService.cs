﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using ParkingLot_WebAPI.Models;


namespace ParkingLot_WebAPI.Services
{
    public class TransactionsService
    {

        public IEnumerable<string> GetTransactions()
        {
            string[] log = { String.Empty };
            try
            {
                log = File.ReadAllText(Settings.LoggingFilePath).Split("\r\n");
            }
            catch (Exception e)
            {
                Console.WriteLine("Seems somethins goes wrong: " + e.Message);
                    
            }
            return log;
        }


        public Transaction GetTransaction(Guid id)
        {
            var transactions = Parking.Source.GetTransactions;
            var item = transactions.First(t => t.VehicleId == id);
            return item;
        }


        public IEnumerable<Transaction> GetLastMinuteTransactions()
        {
            return Parking.Source.TransactionContainerForLastMinute;
        }


        public void TopUpVehicleBalance(Guid vehicleId, decimal balance)
        {
            Parking.Source.TopUpVehicleBalance(vehicleId, balance);
        }
    }
}
